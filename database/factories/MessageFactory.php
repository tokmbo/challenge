<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Message;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {
    return [
        'title'     => str_random(10),
        'body'      => str_random(5) . str_random(5) . str_random(5) . str_random(5),
        'password'  => Hash::make('1234', [
                                'memory' => 1024,
                                'time' => 2,
                                'threads' => 2,
                            ])
    ];
});
