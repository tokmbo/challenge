<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Message::create([
        'title'     => str_random(10),
        'body'      => str_random(5) . str_random(5) . str_random(5) . str_random(5),
        'password'  => Hash::make('test', [
                                'memory' => 1024,
                                'time' => 2,
                                'threads' => 2,
                            ])
]);
    }
}
