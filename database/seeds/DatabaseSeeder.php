<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(MessagesTableSeeder::class);
        $messages = factory(App\Models\Message::class, 50)->create();
    }
}
