<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MoveNameAfterIdFix extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function (Blueprint $table) {
            $table->renameColumn('name', 'name_old');
        });

        Schema::table('messages', function(Blueprint $table) {
            $table->string('name')->nullable()->after('id');
        });

        DB::table('messages')->update([
            'name' => DB::raw('name_old')   
        ]);

        Schema::table('messages', function(Blueprint $table) {
            $table->dropColumn('name_old');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function (Blueprint $table) {
            //
        });
    }
}
