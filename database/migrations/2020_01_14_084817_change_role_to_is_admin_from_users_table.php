<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRoleToIsAdminFromUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_admin')->default(0);
        });

        $users = DB::table('users')->select('role', 'is_admin')->get();

        $i = 1;
        foreach ($users as $user){
            DB::table('users')
                ->where('role', 1)
                ->update([
                    "is_admin" => true
            ]);
            $i++;
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['role']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('role')->default(2);
        });

        $users = DB::table('users')->select('role', 'is_admin')->get();

        $i = 1;
        foreach ($users as $user){
            DB::table('users')
                ->where('is_admin', 1)
                ->update([
                    "role" => 1
            ]);
            $i++;
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['isadmin']);
        });
    }
}
