<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// admin -> role 1
// member ->role 2

Route::resource('/', 'MessageController', [
    'except' => [ 'create', 'edit' ]
]);

Route::post('/destroy', 'MessageController@destroy')->name('destroy');
Route::post('/update', 'MessageController@update')->name('update');
Route::post('/check', 'MessageController@check')->name('check');

Auth::routes(['verify' => true]);
Route::post('/register-confirm', 'Auth\RegisterController@confirm')->name('confirm');
Route::post('/register-success', 'Auth\RegisterController@create')->name('create');

Route::get('/logout', function() {
    Auth::logout();
    return redirect('/');
});

Route::group(
    [
        'prefix'        => 'admin',
        'middleware'    => 'admin'
    ],
    function() {
        Route::get('/', 'DashboardController@index');
        Route::get('/search', 'DashboardController@search')->name('search');
        Route::post('/delete-image', 'DashboardController@deleteImage')->name('delete-image');
        Route::post('/restore', 'DashboardController@restore')->name('restore');
        Route::post('/delete', 'DashboardController@delete')->name('delete');
        Route::post('/multiple-delete', 'DashboardController@multipleDelete')->name('multiple-delete');
        // Route::get('/search', 'AdminController@index');
    }
);