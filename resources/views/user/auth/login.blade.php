@extends('user.layouts.auth')

@section('content')
    <div class="box login-box">
        <div class="login-box-head">
            @if ($errors->all())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <h1 class="mb-5">Login</h1>
            <p class="text-lgray">Please login to continue...</p>
        </div>
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="login-box-body">
                <div class="form-group">
                    <input type="text" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
            </div>
            <div class="login-box-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
@endsection
