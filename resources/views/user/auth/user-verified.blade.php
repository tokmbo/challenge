@extends('user.layouts.auth')

@section('content')
    <div class="box login-box text-center">
        <div class="login-box-head">
            <h1>Member Verified</h1>
        </div>
        <div class="login-box-body">
            <p>Thank you for your membership register.<br/>
            Membership is now complete.</p>
        </div>
        <div class="login-box-footer">
            <div class="text-center">
                <a href="{{ route('index') }}" class="btn btn-primary">Back to Home</a>
            </div>
        </div>
    </div>
@endsection
