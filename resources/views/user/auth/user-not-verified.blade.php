@extends('user.layouts.auth')

@section('content')
    <div class="box login-box text-center">
        <div class="login-box-head">
            <h1>Member Not Verified</h1>
        </div>
        <div class="login-box-body">
            <p>Your membersthip still not verified.<br/>
            Please verify first, then try login again.</p>
        </div>
        <div class="login-box-footer">
            <div class="text-center">
                <a href="{{ route('login') }}" class="btn btn-primary">Login</a>
            </div>
        </div>
    </div>
@endsection
