@extends('user.layouts.auth')

@section('content')
    <div class="box login-box">
        <div class="login-box-head">
            @if ($errors->all())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </div>
            @endif
            <h1 class="mb-5">Register</h1>
            <p class="text-lgray">Please fill the information below...</p>
        </div>
        <form method="POST" action="{{ route('confirm') }}">
            {{ csrf_field() }}
            <div class="login-box-body">
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Name" value="{{ $data['name'] ?? old('name') }}">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="email" placeholder="E-mail" value="{{ $data['email'] ?? old('email') }}">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Password" value="{{ $data['password'] ?? old('password') }}">
                </div>
            </div>
            <div class="login-box-footer">
                <div class="text-right">
                    <a type="button" href="{{ url('/') }}" class="btn btn-default">Back to Homepage</a>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </form>
    </div>
@endsection
