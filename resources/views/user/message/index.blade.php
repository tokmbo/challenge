@extends('user.layouts.app')

@section('content')
    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 bg-white p-30 box">
                    <div class="text-center">
                        <h1 class="text-green mb-30"><b>Level 8 Challenge</b></h1>
                    </div>
                    <form method="post" action="{{ route('store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{ $errors->hasBag('post') ? old('name') : (Auth::user()->name ?? '') }}" autocomplete="name">
                            <p class="small text-danger mt-5">{{ $errors->post->first('name') }}</p>
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" value="{{ $errors->hasBag('post') ? old('title') : '' }}" autocomplete="title">
                            <p class="small text-danger mt-5">{{ $errors->post->first('title') }}</p>
                        </div>
                        <div class="form-group">
                            <label>Body</label>
                            <textarea rows="5" name="body" class="form-control">{{ $errors->hasBag('post') ? old('body') : '' }}</textarea>
                            <p class="small text-danger mt-5">{{ $errors->post->first('body') }}</p>
                        </div>
                        <div class="form-group">
                            <label>Choose image from your computer :</label>
                            <div class="input-group">
                                <input type="text" class="form-control upload-form" value="No file chosen" autocomplete="file-upload" readonly>
                                <span class="input-group-btn">
                                    <span class="btn btn-default btn-file">
                                        <i class="fa fa-folder-open"></i>&nbsp;Browse 
                                        <input type="file" name="image" multiple>
                                    </span>
                                </span>
                            </div>
                            <p class="small text-danger mt-5">{{ $errors->post->first('image') }}</p>
                        </div>
                        @guest
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" autocomplete="new-password" class="form-control">
                                <p class="small text-danger mt-5">{{ $errors->post->first('password') }}</p>
                            </div>
                        @endguest
                        <div class="text-center mt-30 mb-30">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                    @foreach($messages as $message)
                        <div class="post">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h2 class="mb-5 text-green"><b>{{ $message->title }}</b></h2>
                                </div>
                                <div class="pull-right text-right">
                                    <p class="text-lgray">
                                        {{ $message->created_at->format('d-m-Y') }}
                                        <br/>
                                        <span class="small">{{ $message->created_at->format('H:i') }}</span>
                                    </p>
                                </div>
                            </div>
                            <h4 class="mb-20">
                                {{ $message->name ?? "No Name" }} 
                                <span class="text-id">
                                    {{ $message->user_id ? "(membership) [ID: {$message->user_id}]" : "(non membership)" }}
                                </span>
                            </h4>
                            <p>{!! nl2br(e($message->body)) !!}</p>
                            <div class="img-box my-10">
                                <img class="img-responsive img-post" src="{{ $message->thumbnailUrl() }}" alt="image">
                            </div>
                            @if(Auth::user() && Auth::id() === $message->user_id)
                                <form class="form-inline mt-50" action="{{ route('check') }}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $message->id }}">
                                    <button type="submit" class="btn btn-default mb-2" name="action" value="edit">
                                        <i class="fa fa-pencil p-3"></i>
                                    </button>
                                    <button type="submit" class="btn btn-danger mb-2 deleteModal" name="action" value="delete">
                                        <i class="fa fa-trash p-3"></i>
                                    </button>
                                </form>
                            @elseif(Auth::guest() && empty($message->user_id))
                                <form class="form-inline mt-50" action="{{ route('check') }}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group mx-sm-3 mb-2">
                                        <label for="inputPassword2" class="sr-only">Password</label>
                                        <input type="password" class="form-control" placeholder="Password "name="password" autocomplete="rutjfkde">
                                        <input type="hidden" name="id" value="{{ $message->id }}">
                                    </div>
                                    <button type="submit" class="btn btn-default mb-2" name="action" value="edit">
                                        <i class="fa fa-pencil p-3"></i>
                                    </button>
                                    <button type="submit" class="btn btn-danger mb-2 deleteModal" name="action" value="delete">
                                        <i class="fa fa-trash p-3"></i>
                                    </button>
                                </form>
                            @endif
                        </div>
                    @endforeach
                    <div class="text-center mt-30">
                        {{ $messages->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection