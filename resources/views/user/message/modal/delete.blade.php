@if ($msg)
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    @if ($error)
                        <span class="text-danger">{{ $error['body'] }}</span>
                    @else
                        <h4 class="modal-title" id="myModalLabel">Are You Sure Want to Delete This Data?</h4>
                    @endif
                </div>
                <div class="modal-body pad-20">
                    <h3 class="mb-5 text-green"><b>{{ $msg->title }}</b></h3>
                    <p>{!! nl2br(e($msg->body)) !!}</p>
                    <p class="text-lgray text-right">
                        {{ $msg->created_at->format('d-m-Y') }}
                        <br/>
                        <span class="small">{{ $msg->created_at->format('H:i') }}</span>
                    </p>
                </div>
                <div class="modal-footer">
                    @if ($error)
                        <form action="{{ route('check') }}" method="POST" class="form-inline mt-50">
                            {{ csrf_field() }}
                            @if (strlen($error['body']) !== 56)
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            @else
                                <div class="form-group mx-sm-3 mb-2 pull-left">
                                    <label for="inputPassword2" class="sr-only">Password</label>
                                    <input type="password" class="form-control" placeholder="Password" name="password" autocomplete="new-password">
                                    <input type="hidden" name="id" value="{{ $msg->id }}">
                                    <button type="submit" class="btn btn-danger mb-2" name="action" value="delete"><i class="fa fa-trash p-3"></i></button>
                                </div>
                            @endif
                        </form>
                    @else
                        <form action="{{ route('destroy') }}" method="POST" class="form-inline mt-50">
                            {{ csrf_field() }}
                            <input type="hidden" name="page" value="{{ $messages->currentPage() }}">
                            <input type="hidden" name="lastPage" value="{{ $messages->lastPage() }}">
                            <input type="hidden" name="id" value="{{ $msg->id }}">
                            @guest
                                <input type="hidden" name="password" value="{{ $msg->password }}">
                            @endguest
                            <button type="submit" class="btn btn-danger">Yes</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif