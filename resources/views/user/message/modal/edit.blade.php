@if ($msg)
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    @if ($error)
                        <span class="text-danger">{{ $error['body'] }}</span>
                    @else
                        <h4 class="modal-title" id="myModalLabel">Edit Item</h4>
                    @endif
                </div>
                @if ($error)
                    <div class="modal-body pad-20">
                        <h3 class="mb-5 text-green"><b>{{ $msg->title }}</b></h3>
                        <h4>{{ $msg->name }}</h4>
                        <p>{!! nl2br(e($msg->body)) !!}</p>
                        <div class="img-box my-10">
                            <img class="img-responsive img-post" src="{{ $msg->imgUrl() }}" alt="image">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <form action="{{ route('check') }}" method="POST" class="form-inline mt-50">
                            {{ csrf_field() }}
                            @if (strlen($error['body']) !== 56)
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            @else
                                <div class="form-group mx-sm-3 mb-2 pull-left">
                                    <label for="inputPassword2" class="sr-only">Password</label>
                                    <input type="password" class="form-control" placeholder="Password" name="password" autocomplete="new-password">
                                    <input type="hidden" name="id" value="{{ $msg->id }}">
                                    <button type="submit" class="btn btn-default mb-2" name="action" value="edit"><i class="fa fa-pencil p-3"></i></button>
                                </div>
                            @endif
                        </form>
                    </div>
                @else
                    <form method="post" action="{{ route('update') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal-body">
                            @guest
                                <input type="hidden" name="password" value="{{ old('password', $msg->password) }}">
                            @endguest
                            <input type="hidden" name="id" value="{{ old('id', $msg->id) }}">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name" value="{{ old('name', $msg->name ?? '') }}">
                                <p class="small text-danger mt-5">{{ $errors->edit->first('name') }}</p>
                            </div>
                            <div class="form-group">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title" value="{{ old('title', $msg->title) }}">
                                <p class="small text-danger mt-5">{{ $errors->edit->first('title') }}</p>
                            </div>
                            <div class="form-group">
                                <label>Body</label>
                                <textarea rows="5" name="body" class="form-control">{{ old('body', $msg->body) }}</textarea>
                                <p class="small text-danger mt-5">{{ $errors->edit->first('body') }}</p>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-4">
                                    <img class="img-responsive" alt="" src="{{ $msg->thumbnailUrl() }}">
                                </div>
                                <div class="col-md-8 pl-0">
                                    <label>Choose image from your computer :</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control upload-form" value="No file chosen" readonly autocomplete="nofilechosen">
                                        <span class="input-group-btn">
                                            <span class="btn btn-default btn-file">
                                                <i class="fa fa-folder-open"></i>&nbsp;Browse <input type="file" name="image" multiple>
                                            </span>
                                        </span>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="priority" value="delete">Delete image
                                        </label>
                                    </div>
                                </div>
                                <p class="small text-danger mt-5">{{ $errors->edit->first('image') }}</p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endif