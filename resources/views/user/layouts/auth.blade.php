<html>
    <head>
        <title>Timedoor Challenge - Level 6</title>

        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/style.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('user/css/tmdrPreset.css') }}">

        <script type="text/javascript" src="{{ URL::asset('user/js/jquery.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('user/js/bootstrap.min.js') }}"></script>

        <script>
            // INPUT TYPE FILE
            $(document).on('change', '.btn-file :file', function() {
                var input   = $(this),
                numFiles    = input.get(0).files ? input.get(0).files.length : 1,
                label       = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            $(document).ready( function() {
                $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
                    var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if( input.length ) {
                        input.val(log);
                    } else {
                        if( log ) alert(log);
                    }
                });
            });
        </script>
    </head>

    <body id="login">
        @yield('content')
    </body>
</html>