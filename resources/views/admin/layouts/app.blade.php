<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Timedoor Admin | Dashboard</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <link rel="stylesheet" href="{{ URL::asset('adm/plugin/bootstrap/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/plugin/font-awesome/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/plugin/Ionicons/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/css/admin.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/css/tmdrPreset.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/css/custom.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/css/skin.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/plugin/bootstrap-datepicker/bootstrap-datetimepicker.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/plugin/daterangepicker/daterangepicker.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/plugin/datatable/datatables.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('adm/plugin/selectpicker/bootstrap-select.css') }}">
        
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <a href="/admin" class="logo">
                    <span class="logo-mini"><b>T</b>D</span>
                    <span class="logo-lg"><b>Timedoor</b> Admin</span>
                </a>
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <span class="hidden-xs">Hello, {{ Auth::user()->name }} </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        <img src="img/user-ico.png" class="img-circle" alt="User Image">
                                        <p>
                                            Administrator
                                        </p>
                                    </li>
                                    <li class="user-footer">
                                        <div class="text-right">
                                            <a href="{{ route('logout') }}" class="btn btn-danger btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>

            <aside class="main-sidebar">
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="active">
                            <a href="/admin"><i class="fa fa-dashboard"></i> 
                                <span>Dashboard</span>
                            </a>
                        </li>
                    </ul>
                </section>
            </aside>

            <div class="content-wrapper">
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section>
            <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 0.1.0
                </div>
                <strong>Copyright &copy; 2019 <a href="https://timedoor.net" class="text-green">Timedoor Indonesia</a>.</strong> All rights
                reserved.
            </footer>

            @yield('modals')
        </div>

        <script src="{{ URL::asset('adm/plugin/jquery/jquery.js') }}"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="{{ URL::asset('adm/plugin/jquery/jquery-ui.min.js') }}"></script>

        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>

        <!-- Bootstrap 3.3.7 -->
        <script src="{{ URL::asset('adm/plugin/bootstrap/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('adm/plugin/moment/moment.min.js') }}"></script>
        <script src="{{ URL::asset('adm/plugin/daterangepicker/daterangepicker.js') }}"></script>
        <script src="{{ URL::asset('adm/plugin/bootstrap-datepicker/bootstrap-datetimepicker.js') }}"></script>
        <script src="{{ URL::asset('adm/js/adminlte.min.js') }}"></script>
        <script src="{{ URL::asset('adm/plugin/datatable/datatables.min.js') }}"></script>
        <script src="{{ URL::asset('adm/plugin/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ URL::asset('adm/plugin/selectpicker/bootstrap-select.js') }}"></script>

        <script>
            // BOOTSTRAP TOOLTIPS
            if ($(window).width() > 767) {
                $(function () {
                $('[rel="tooltip"]').tooltip()
                });
            };
        </script>

        @yield('js');
    </body>
</html>
