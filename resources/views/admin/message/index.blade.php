@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h1 class="font-18 m-0">Timedoor Challenge - Level 9</h1>
                </div>
                <div class="box-body">
                    <div class="bordered-box mb-20">
                        <form method="GET" action="{{ route('search') }}" id="searchForm">
                            <table class="table table-no-border mb-0">
                                <tbody>
                                    <tr>
                                        <td width="80"><b>Title</b></td>
                                        <td><div class="form-group mb-0">
                                            <input type="text" class="form-control" name="title" value="{{ $params['title'] ?? "" }}" id="title">
                                        </div></td>
                                    </tr>
                                    <tr>
                                        <td><b>Body</b></td>
                                        <td><div class="form-group mb-0">
                                            <input type="text" class="form-control" name="body" value="{{ $params['body'] ?? "" }}" id="body">
                                        </div></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-search">
                                <tbody>
                                    <tr>
                                        <td width="80"><b>Image</b></td>
                                        <td width="60">
                                            <label class="radio-inline">
                                            <input type="radio" name="image" value="with" 
                                                @if(!empty($params['image']))
                                                    {{ $params['image'] === "with" ? "checked" : "" }}
                                                @endif> with
                                            </label>
                                        </td>
                                        <td width="80">
                                            <label class="radio-inline">
                                            <input type="radio" name="image" value="without" 
                                                @if(!empty($params['image']))
                                                    {{ $params['image'] === "without" ? "checked" : "" }}
                                                @endif> without
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radio-inline">
                                            <input type="radio" name="image" value="all" 
                                                @if(!empty($params['image']))
                                                    {{ $params['image'] === "all" ? "checked" : "" }}
                                                @else
                                                    {{ "checked" }}
                                                @endif> unspecified
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="80"><b>Status</b></td>
                                        <td>
                                            <label class="radio-inline">
                                            <input type="radio" name="status" value="on"
                                                @if(!empty($params['status']))
                                                    {{ $params['status'] === "on" ? "checked" : "" }}
                                                @endif> on
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radio-inline">
                                            <input type="radio" name="status" value="delete"
                                                @if(!empty($params['status']))
                                                    {{ $params['status'] === "delete" ? "checked" : "" }}
                                                @endif> delete
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radio-inline">
                                            <input type="radio" name="status" value="all"
                                                @if(!empty($params['status']))
                                                    {{ $params['status'] === "all" ? "checked" : "" }}
                                                @else
                                                    {{ "checked" }}
                                                @endif> unspecified
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><button type="submit" class="btn btn-default mt-10"><i class="fa fa-search"></i> Search</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                    <form role="form" method="POST" id="delChecked" action="{{ route('multiple-delete') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="prevUrl" value="{{ $messages->previousPageUrl() }}">
                        <input type="hidden" name="lastPage" value="{{ $messages->lastPage() }}">
                        <input type="hidden" name="total" value="{{ $messages->total() }}">
                        <input type="hidden" name="currentPage" value="{{ $messages->currentPage() }}">

                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" id="checker" onclick="handleClick();"></th>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Body</th>
                                    <th width="200">Image</th>
                                    <th>Date</th>
                                    <th width="50">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @unless(count($messages) > 0)
                                    <tr><td colspan="7" align="center">Item not found!</td></tr>
                                @endunless
                                @foreach($messages as $message)
                                    <tr class="{{ $message->deleted_at ? 'bg-gray-light' : ''}}">
                                        <td>
                                            @unless($message->deleted_at)
                                                <input type="checkbox" name="select[]" value="{{ $message->id }}" onclick="calc()">
                                            @endunless
                                        </td>
                                        <td>{{ $message->id }}</td>
                                        <td>{{ $message->title }}</td>
                                        <td>{{ $message->body }}</td>
                                        <td>
                                            @if($message->image)
                                                <img class="img-prev" src="{{ $message->thumbnailUrl()}}">
                                                @unless($message->deleted_at)
                                                    <a href="#" data-toggle="modal" data-target="#deleteImage" class="btn btn-danger ml-10 btn-img message" rel="tooltip" title="Delete Image" data-id="{{ $message->id }}"><i class="fa fa-trash"></i></a>
                                                @endunless
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>{{ $message->created_at->format('d/m/Y') }}<br><span class="small">{{ $message->created_at->format('H:i:s') }}</span></td>
                                        <td>
                                            @empty($message->deleted_at)
                                                <a href="#" data-toggle="modal" data-target="#deleteMessage" class="btn btn-danger btn-img message" rel="tooltip" title="Delete Message" data-id="{{ $message->id }}"><i class="fa fa-trash"></i></a>
                                            @else
                                                <a href="#" data-toggle="modal" data-target="#restoreMessage" class="btn btn-default message" rel="tooltip" title="Recover" data-id="{{ $message->id }}"><i class="fa fa-repeat"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-default mt-5" data-toggle="modal" id="delConfirm" data-target="#deleteMultiple" disabled>Delete Checked Items</button>
                        <div class="text-center">
                            <nav>
                                {{ $messages->links() }}
                            </nav>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    <div class="modal fade" id="deleteImage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="text-center">
                        <h4 class="modal-title" id="myModalLabel">Delete Image</h4>
                    </div>
                </div>
                <div class="modal-body pad-20">
                    <p>Are you sure want to delete this image?</p>
                </div>
                <div class="modal-footer">
                    <form method="post" action="{{ route('delete-image') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="prevUrl" value="{{ $messages->previousPageUrl() }}">
                        <input type="hidden" name="lastPage" value="{{ $messages->lastPage() }}">
                        <input type="hidden" name="total" value="{{ $messages->total() }}">
                        <input type="hidden" name="currentPage" value="{{ $messages->currentPage() }}">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="restoreMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="text-center">
                        <h4 class="modal-title" id="myModalLabel">Restore Message</h4>
                    </div>
                </div>
                <div class="modal-body pad-20">
                    <p>Are you sure want to restore this message?</p>
                </div>
                <div class="modal-footer">
                    <form method="POST" action="{{ route('restore') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="prevUrl" value="{{ $messages->previousPageUrl() }}">
                        <input type="hidden" name="lastPage" value="{{ $messages->lastPage() }}">
                        <input type="hidden" name="total" value="{{ $messages->total() }}">
                        <input type="hidden" name="currentPage" value="{{ $messages->currentPage() }}">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Restore</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="text-center">
                        <h4 class="modal-title" id="myModalLabel">Delete Data</h4>
                    </div>
                </div>
                <div class="modal-body pad-20">
                    <p>Are you sure want to delete this item?</p>
                </div>
                <div class="modal-footer">
                    <form method="post" action="{{ route('delete') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="">
                        <input type="hidden" name="prevUrl" value="{{ $messages->previousPageUrl() }}">
                        <input type="hidden" name="lastPage" value="{{ $messages->lastPage() }}">
                        <input type="hidden" name="total" value="{{ $messages->total() }}">
                        <input type="hidden" name="currentPage" value="{{ $messages->currentPage() }}">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="deleteMultiple" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <div class="text-center">
                        <h4 class="modal-title" id="myModalLabel">Delete Data</h4>
                    </div>
                </div>
                <div class="modal-body pad-20">
                    <p>Are you sure want to delete this item(s)?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger submit">Delete</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    {{-- pass message id to confirmation modal --}}
    <script type="text/javascript">
        var classname   = document.getElementsByClassName("message");

        var myFunction  = function() {
            var attribute   = this.getAttribute("data-id");
            var name        = document.getElementsByName("id");

            for (var i = 0; i < name.length; i++) {
                name[i].setAttribute('value', attribute);
            }
        };

        for (var i = 0; i < classname.length; i++) {
            classname[i].addEventListener('click', myFunction, false);
        }
    </script>

    {{-- check/uncheck all --}}
    <script type="text/javascript">
        function handleClick() {
            var items   = document.getElementsByName('select[]');
            var checker = document.getElementById('checker');
            for (var i = 0; i < items.length; i++) {
                if (items[i].type == 'checkbox')
                items[i].checked = checker.checked;
            }

            calc();
        }
    </script>

    {{-- disable checkbox if there is no active post/message --}}
    <script type="text/javascript">
        var items       = document.getElementsByName('select[]');
        var checker     = document.getElementById('checker');

        if (items.length == 0) {
            checker.disabled = true;
        }
    </script>

    {{-- submit form for multiple delete --}}
    <script type="text/javascript">
        document.querySelector(".submit").addEventListener("click", (e) => {
            document.getElementById("delChecked").submit(); 
        });
    </script>

    {{-- disable title/body field when they have empty value --}}
    {{-- <script type="text/javascript">
        var ready = (callback) => {
            if (document.readyState != "loading") callback();
            else document.addEventListener("DOMContentLoaded", callback);
        }

        ready(() => { 
            const form = document.getElementById("searchForm");

            form.addEventListener('submit', function (event) {
                var title = document.getElementById('title');
                var body = document.getElementById('body');

                if (title.getAttribute('value') == "") {
                    title.disabled = true;
                }

                if (body.getAttribute('value') == "") {
                    body.disabled = true;
                }
            }); 
        });
    </script> --}}

    {{-- enable/disable "delete checked items" --}}
    <script type="text/javascript">
        var button      = document.getElementById('delConfirm');

        function calc() {
            var textinputs  = document.getElementsByName('select[]');
            var empty       = [].filter.call( textinputs, function( el ) {
                return !el.checked
            });

            if (textinputs.length == empty.length) {
                button.disabled = true;
            } else {
                button.disabled = false;
            }
        }
    </script>
@endsection