<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class Member
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return $next($request);
        }
        
        if (Auth::user()->is_admin) {
            return redirect('/');
        } else {
            return $next($request);
        }
    }
}
