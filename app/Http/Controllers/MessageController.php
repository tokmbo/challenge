<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Message;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Image;
use File;
use Session;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('member');
    }

    public function index()
    {
        $messages = Message::orderBy('id', 'desc')->paginate(10);

        return view('user.message.index', compact('messages'));
    }

    public function store(StoreUserRequest $request)
    {
        $message = new Message($request->input());

        if (Auth::user()) {
            $message->user_id = Auth::id();
        } else {
            $message->hashPassword();
        }

        $message->image = ($image = $request->image) ? $this->setImage($image) : null;

        $message->save();

        return redirect('/');
    }

    public function update(UpdateUserRequest $request)
    {
        if ($request->validated()) {
            $message = Message::findOrFail($request->id);

            $this->checkAuthorize($request->password, $message);

            $data = request()->except(['_token', 'id', 'priority', 'password']);

            if (($request->priority)) {
                $message->imageDelete($message->image);
            } else {
                if ($request->image) {
                    $message->imageDelete($message->image);

                    $data['image'] = $this->setImage($request->image);
                }
            }

            $message->update($data);

            return redirect()->back();
        }
    }

    public function destroy(Request $request)
    {
        $message = Message::findOrFail($request->id);

        $this->checkAuthorize($request->password, $message);

        $message->delete();

        if ($request->page === $request->lastPage) {
            return redirect('/?page=' . --$request->page);
        } else {
            return redirect()->back();
        }
    }

    public function check(Request $request)
    {
        $message = Message::findOrFail($request->id);

        if (Auth::guest()) {
            if (!$message->password) {
                $error = ["body" => "This message can't {$request->action}, because this message has not been set password."];
            } else {
                if (!Hash::check($request->password, $message->password)) {
                    $error = ["body" => "The password you entered do not match. Please try again."];
                }
            }

            $message['password'] = $request->password;
        }

        $data    = [
            "{$request->action}Modal"   => "{$request->action}Modal",
            'message'                   => $message,
            isset($error) ? 'err' : 'n' => isset($error) ? $error : null
        ];

        return redirect()->back()->with($data);
    }

    public function setImage($image)
    {
        $fileName   = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
        $extension  = $image->getClientOriginalExtension();
        $imageName  = "{$fileName}_" . time() . ".{$extension}";

        $thumbImage = Image::make($image->getRealPath())
                        ->fit(200);
        $thumbPath  = public_path() . Message::$pathThumb;
        $oriPath    = public_path() . Message::$pathImage;
        
        if( ! \File::isDirectory($thumbPath) ) { 
            \File::makeDirectory($thumbPath, 493, true);
        }

        Image::make($thumbImage)->save($thumbPath . $imageName);
        Image::make($image)->save($oriPath . $imageName);

        return $imageName;
    }

    public function checkAuthorize($plain, $message)
    {
        if (Auth::guest()) {
            if (!Hash::check($plain, $message->password)) {
                abort(403, 'Unauthorized action.');
            }
        } else {
            if (Auth::id() !== $message->user_id) {
                abort(403, 'Unauthorized action.');
            }
        }
    }
}