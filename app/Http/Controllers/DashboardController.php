<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $messages = Message::withTrashed()->orderBy('id', 'desc')->paginate(10);
        
        return view('admin.message.index', compact('messages'));
    }

    public function search(Request $request)
    {
        $params = $request->input();

        $query = Message::withTrashed()->where([
            ['title', 'LIKE', "%{$request->title}%"],
            ['body', 'LIKE', "%{$request->body}%"],
        ]);

        if ($request->status === 'on') {
            $query->whereNull('deleted_at');
        }elseif ($request->status === 'delete') {
            $query->whereNotNull('deleted_at');
        }

        if ($request->image === 'with') {
            $query->whereNotNull('image');
        }elseif ($request->image === 'without') {
            $query->whereNull('image');
        }

        $messages = $query->orderBy('id', 'desc')->paginate(10);
        $messages->appends($params);

        return view('admin.message.index', compact('messages', 'params'));
    }

    public function deleteImage(Request $r)
    {
        $message    = Message::findOrFail($r->id);

        $message->imageDelete($messages->image);

        return ($this->target($r) ? redirect($r->prevUrl) : redirect()->back());
    }

    public function restore(Request $r)
    {
        Message::whereId($r->id)->restore();

        return ($this->target($r) ? redirect($r->prevUrl) : redirect()->back());
    }

    public function delete(Request $r)
    {
        Message::findOrFail($r->id)->delete();

        return ($this->target($r) ? redirect($r->prevUrl) : redirect()->back());
    }

    public function multipleDelete(Request $r)
    {
        Message::whereIn('id', $r->select)->get()->each(function($msg) {
            $msg->delete();
        });

        return ($this->target($r) ? redirect($r->prevUrl) : redirect()->back());
    }

    public function target(Request $r)
    {
        $total = $r->select ? $r->total-count($r->select) : $r->total-1;

        if ($r->currentPage === $r->lastPage) {
            if ($total % 10 === 0 && $total !== 0) {
                return true;
            }
        }

        return false;
    }
}
