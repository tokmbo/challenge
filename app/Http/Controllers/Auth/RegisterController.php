<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterConfirmRequest;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     * This is an override function.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm($request = null)
    {
        return view('user.auth.register');
    }

    /**
     * Handle a registration request for the application.
     * This is an override function.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        if ($request->action === 'back') {
            return view('user.auth.register', ['data' => $request->all()]);
        }

        $user = new User($request->except(['_token']));

        $user->hashPassword();
        $user->save();
        $user->sendEmailVerificationNotification($request->name);

        return view('user.auth.register-success');
    }

    /**
     * Show the data confirmation view.
     *
     * @param  \App\Http\Requests\RegisterConfirmRequest  $request
     * @return \Illuminate\Http\Response
     */
    protected function confirm(RegisterConfirmRequest $r)
    {
        return view('user.auth.register-confirm', ['input' => (object) $r->all()]);
    }
}