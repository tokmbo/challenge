<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

use App\Models\Message;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'nullable|string|between:3,16',
            'title'     => 'required|string|between:10,32',
            'body'      => 'required|string|between:10,200',
            'image'     => 'image|mimes:jpeg,jpg,png,gif|max:1024'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $message = Message::find($this->id);

        $message['password'] = $this->password;

        return redirect()->back()
            ->withErrors($validator, 'edit')
            ->with([
                'editModal'     => 'editModal',
                'msg'           => $message
            ]);
    }
}
