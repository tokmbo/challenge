<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class StoreUserRequest extends FormRequest
{
    protected $errorBag = 'post';
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'nullable|string|between:3,16',
            'title'     => 'required|string|between:10,32',
            'body'      => 'required|string|between:10,200',
            'password'  => 'nullable|numeric|min:4',
            'image'     => 'image|mimes:jpeg,jpg,png,gif|max:1024'
        ];
    }
}
