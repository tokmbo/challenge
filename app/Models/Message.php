<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class Message extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'body',
        'password',
        'image',
        'name'
    ];

    public static $pathImage = "/storage/images/message/";
    public static $pathThumb = "/storage/images/message/thumbnail/";

    public function imgUrl()
    {
        if ($this->image) {
            return self::$pathImage . $this->image;
        } else {
            return "http://via.placeholder.com/500x500";
        }
    }

    public function thumbnailUrl()
    {
        if ($this->image) {
            return self::$pathThumb . $this->image;
        } else {
            return "http://via.placeholder.com/200x200";
        }
    }

    public function hashPassword()
    {
        if ($this->password) {
            $this->password = Hash::make($this->password, [
                                'memory' => 1024,
                                'time' => 2,
                                'threads' => 2,
                            ]);
        }
    }

    public function delete()
    {
        $tempImage = $this->image;

        parent::delete();

        $this->imageDelete($tempImage);
    }

    public function imageDelete($image)
    {
        $this->image = null;
        $this->save();

        if (! empty($image)) {
            unlink(public_path() . self::$pathImage . $image);
            unlink(public_path() . self::$pathThumb . $image);
        }
    }
}
